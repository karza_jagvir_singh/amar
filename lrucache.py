#VARIABLES:
#llist: Linked List to contain the Cache
#hmap: Used to traverse throught the cache in O(1) time
#LRUNode: This keeps track of the least used node at any given timestamp.

#------------------------------Conditions

#LRU Cache (Least Recently Used Cache)
#Generally doubly linked list and hash map together are to be used.
#Get = o(1), put = o(1), remove = o(1).
#Space = o(n).

#-------------------------------Thoughts before I started coding

#Every time an element is called, it will be the most recently used one.
#Every time, the least recently used element should be removed in o(1). This means, LRU must always have a pointer or a way to reach it in a constant number of steps.
#Just because a number is removed from cache, it should not be gone from the ram. This is a side quest tho.
#For one to recognise the LRU, one should have a way of ordering the elements in the cache. But this must also be done in a constant time, because insert is o(1) and remove is too.

#For a cache to have an order, we can use something like this: Whenever a number is called, we will move the number to the front of the linked list.
#This way, the last number of the linked list is always the least recently used.
#Now, the problem is to reach the number in the linked list a constant time.
#For this, we can do this: Create a map with the number as the key and it's linkedlist element as the value. Now, we can get the linkedlist address directly and proceed to it in o(1).
#Also, remember, the cache should always be of a limited size. So, whenever the LRU is removed, we need to make sure that the value in linkedlist and map is removed and LRU should also be updated.

#-------------------------------Thoughts when I am coding

#After writing the code, I think there is no need for a doubly linked list for this. But I am just leaving it here since there won't be any problems in the complexities anyway.

#Now, how do I keep track of the Least used element? As I said the linked list is ordered. But we have no way of reaching the end in o(1) time.
#For us to escape this problem, we need to create a different node that always points to the end of the linked list. This is the LRUNode. In this case we can just store the Value.
#I wrote above that the doubly linked list is not necessary but now, it is needed to keep track of LRUNode.
class Node:
    def __init__(self, intval = None):
        self.val = intval
        self.next = None
        self.prev = None

class LinkedList:
    def __init__(self):
        self.head = None

def get(llist = None, hmap = None, intval = None):
    LRUNode = -1
    if intval in hmap:
        print(intval)
        if hmap[intval].next == None:
            #print("entered")
            if hmap[intval].prev is None:
                #print("this is the first element")
                pass
            else:
                #print("went here")
                LRUNode = hmap[intval].prev.val
                #print(LRUNode)
        else:
            print(hmap[intval].next.val)
            hmap[intval].next.prev = hmap[intval].prev
        if hmap[intval].prev is not None:
            hmap[intval].prev.next = hmap[intval].next
        hmap[intval].prev = None
        hmap[intval].next = llist.head
        llist.head = hmap[intval]
    else:
        print("-1")
    return LRUNode

#To add a new val in the cache.
def put(llist = None, hmap = None, intval = None):
    #print("Adding the number to the cache")
    LRUNode = -1
    if intval in hmap:
        if hmap[intval].next is None:
            LRUNode = hmap[intval].prev
        else:
            hmap[intval].next.prev = hmap[intval].prev
        if hmap[intval].prev is not None:
            hmap[intval].prev.next = hmap[intval].next
        hmap[intval].prev = None
        hmap[intval].next = llist.head
        llist.head = hmap[intval]
    else:
        hmap[intval] = Node(intval)
        if len(hmap) == 1:
            llist.head = hmap[intval]
            LRUNode = hmap[intval].val
            #print(LRUNode)
        else:
            hmap[intval].next = llist.head
            llist.head.prev = hmap[intval]
            llist.head = hmap[intval]
    return LRUNode
    

llist = LinkedList()
llist.head = Node()
hmap = {}
LRUNode = -1

while True:
    option = input("What do u want to do (get/set/show) ")
    if option == "get":
        intval = input("please give a positive value ")
        temp = get(llist, hmap, intval)
        if temp != -1:
            LRUNode = temp
        #print("the head of the cache is ", llist.head.val)
        #print("Least recently used node is ", LRUNode)
    if option == "set":
        intval = input("please give a positive value ")
        temp1 = put(llist, hmap, intval)
        if temp1 != -1:
            LRUNode = temp1
        #print("the head of the cache is ", llist.head.val)
        #print("Least recently used node is ", LRUNode)
        if len(hmap) >= 10:
            temp = LRUNode
            LRUNode = hmap[LRUNode].prev.val
            del hmap[temp]
    if option == "show":
        node = llist.head
        while node:
            print(node.val)
            node = node.next